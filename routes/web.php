<?php

use App\Http\Controllers\HomeComtroller;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'home'])->name('dashboard');


Route::resource('/notes', 'App\Http\Controllers\NotesController');
Route::post('/notes/toggle-status/{note}', [NotesController::class, 'toggleStatus'])->middleware(['auth']);
Route::post('/notes/{note}/comment', [NotesController::class, 'comment'])->middleware(['auth']);

require __DIR__ . '/auth.php';
