<?php

namespace App\Policies;

use App\Models\Note;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Note $note)
    {
        return ($user && $note->user_id == $user->id);
    }

    public function toggleStatus(User $user, Note $note)
    {
        return $note->user_id == auth()->id();
    }
}
