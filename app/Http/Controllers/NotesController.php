<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Requests\NoteRequest;
use App\Models\Note;
use Carbon\Carbon;
use Illuminate\Support\Str;

class NotesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth'])->except(['show']);
    }

    public function index()
    {
        $notes = Note::where('user_id', auth()->id())->get();
        return view('notes/my-notes', compact('notes'));
    }

    public function create()
    {
        return view('notes/add');
    }

    public function store(NoteRequest $request)
    {
        $data = $request->validated();
        $data['published'] = $data['published'] == 'on';
        $data['slug'] = Str::slug(Str::words($data['text'] . "-" , 5). Carbon::now()->timestamp);
        Note::create($data + ['user_id' => auth()->id()]);
        return redirect('/notes');
    }

    public function destroy(Note $note)
    {
        $note->delete();
        return response()->json(['message' => 'Successfully Deleted']);
    }

    public function show(Note $note)
    {
        if (!$note->published)
            $this->authorize('show', [$note]);
        $note = Note::with('comments')->where('slug', $note->slug)->firstOrFail();
        return view('notes/show', compact('note'));
    }

    public function toggleStatus(Note $note)
    {
        $this->authorize('toggleStatus', [$note]);
        $note->published = !$note->published;
        $note->save();
        return response()->json(['message' => 'Operation Done Successfully']);
    }

    public function comment(Note $note, CommentRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = auth()->id();
        $note->comments()->create($data);
        return response()->json(['message' => 'Commented Successfully', 'text' => $data['text']]);
    }
}
