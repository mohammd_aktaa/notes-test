<?php

namespace App\Http\Controllers;

use App\Models\Note;

class HomeController extends Controller
{
    public function home()
    {
        $notes = Note::with('user')->inRandomOrder()->isPublished()->get();
        return view('notes/all', compact('notes'));
    }
}
