<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ \Illuminate\Support\Str::words($note->text,5) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <p class="px-6 py-4">{!! $note->text !!}</p>
            </div>
        </div>
    </div>
    <div class="pb-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <h2 class="px-3 py-3 border-b bg-gray-50 font-semibold text-xl text-gray-800 leading-tight">
                    Comments
                </h2>
                <ul class="comment-list">
                    @foreach($note->comments as $comment)
                        <li class="px-4 py-4 border-b">{{$comment->text}}</li>
                    @endforeach
                    @auth
                        <li class="px-4 py-3 comment-form">
                            <form action="" class="flex items-center">
                                @csrf
                                <input type="text" name="text" placeholder="Write your comment here..." required
                                       class="w-10/12 flex-1 appearance-none rounded shadow p-3 text-grey-dark mr-2 focus:outline-none">
                                <button type="submit"
                                        class="w-2/12 appearance-none bg-gray-800 text-white text-base font-semibold tracking-wide uppercase p-3 rounded shadow hover:bg-gray-600 transition">
                                    Comment
                                </button>
                            </form>
                        </li>
                    @endauth
                </ul>
            </div>
        </div>
    </div>
    @push('styles')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    @endpush
    @push('scripts')
        <script>
            $('.comment-form form').off('submit').on('submit', function ($event) {
                $event.preventDefault();
                let data = $(this).serialize();
                $.ajax({
                    type: 'post',
                    url: '{{route('notes.index')}}/' + '{{$note->slug}}/comment',
                    data,
                    success(res) {
                        $('.comment-form form input[type=text]').val('')
                        $('.comment-form').before(`<li class="px-4 py-3 border-b">${res.text}</li>`)
                    }
                })
            })
        </script>
    @endpush
</x-app-layout>
