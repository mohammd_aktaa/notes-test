<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Notes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="flex flex-wrap">
                @foreach($notes as $note)
                    <div class="px-2 mb-3 w-full sm:w-1/2 ">
                        <a target="_blank" href="{{route('notes.show',$note->slug)}}"
                           class="bg-white transition hover:bg-gray-50 shadow-sm rounded-lg cursor-pointer block">
                            <p class="py-6 px-3 border-b border-gray-200">
                                {{\Illuminate\Support\Str::words($note->text,8)}}
                            </p>
                            <span class="py-1 px-3 block text-sm">By <span
                                    class="text-blue-400">{{$note->user->name}}</span></span>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</x-app-layout>
