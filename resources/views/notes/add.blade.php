<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Note') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <form action="{{route('notes.store')}}" enctype="multipart/form-data" method="post"
                      class="p-6 bg-white border-b border-gray-200">
                    @csrf
                    <div>
                        <textarea name="text" class="border p-2 mt-3 w-full rounded" rows="10">{{old('text')}}</textarea>
                        @if ($errors->has('text'))
                            <small class="text-sm text-red-500">{{$errors->first('text')}}</small>
                        @endif
                    </div>
                    <div class="flex space-x-2 mt-2">
                        <input type="hidden" name="published" value="off">
                        <input type="checkbox" name="published" id="is-published" class="inline-block">
                        <label for="is-published" class="text-gray-600 text-sm">Is Published?</label>
                    </div>
                    @if ($errors->has('published'))
                        <small class="text-sm text-red-500">{{$errors->first('published')}}</small>
                    @endif
                    <div class="flex items-center justify-end mt-4">

                        <x-button type="submit" class="ml-3">
                            {{ __('Submit') }}
                        </x-button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
